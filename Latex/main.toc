\contentsline {chapter}{\numberline {1}Overview}{2}
\contentsline {section}{\numberline {1.1}History of Google}{2}
\contentsline {section}{\numberline {1.2}Products By Google}{3}
\contentsline {section}{\numberline {1.3}Selection Process}{4}
\contentsline {section}{\numberline {1.4}Why Sydney University?}{4}
\contentsline {chapter}{\numberline {2}Major}{6}
\contentsline {section}{\numberline {2.1}Computer Science}{6}
\contentsline {subsection}{\numberline {2.1.1}Overview}{6}
\contentsline {subsection}{\numberline {2.1.2}Compared to other majors}{6}
\contentsline {subsubsection}{Similarities and differences between Software Development and other Advanced Computing Majors}{6}
\contentsline {subsubsection}{Differences}{6}
\contentsline {subsection}{\numberline {2.1.3}Roles}{6}
\contentsline {subsubsection}{AI Research Scientist}{6}
\contentsline {subsection}{\numberline {2.1.4}Roles}{7}
\contentsline {subsubsection}{Machine Learning Product Specialist, Google Cloud}{7}
\contentsline {section}{\numberline {2.2}Information System}{8}
\contentsline {subsection}{\numberline {2.2.1}Overview}{8}
\contentsline {subsection}{\numberline {2.2.2}Career Progression}{8}
\contentsline {subsection}{\numberline {2.2.3}Compared to other majors}{8}
\contentsline {subsubsection}{Similarities}{8}
\contentsline {subsubsection}{Differences}{8}
\contentsline {subsection}{\numberline {2.2.4}Roles \cite {dev-google-careers}}{9}
\contentsline {subsubsection}{Systems Administrator}{9}
\contentsline {subsubsection}{Information Security Engineer}{9}
\contentsline {section}{\numberline {2.3}Software Development}{11}
\contentsline {subsection}{\numberline {2.3.1}Overview}{11}
\contentsline {subsection}{\numberline {2.3.2}Compared to other majors}{11}
\contentsline {subsubsection}{Similarities}{11}
\contentsline {subsubsection}{Differences}{11}
\contentsline {subsection}{\numberline {2.3.3}Roles \cite {dev-google-careers}}{11}
\contentsline {subsubsection}{Software Engineer}{11}
\contentsline {subsubsection}{Security Software Engineer}{12}
\contentsline {subsubsection}{Infrastructure Software Engineer}{12}
\contentsline {subsubsection}{Front End Software Engineer}{13}
\contentsline {subsubsection}{Mobile App Software Engineer}{13}
\contentsline {section}{\numberline {2.4}Data Science}{14}
\contentsline {subsection}{\numberline {2.4.1}Overview}{14}
\contentsline {subsection}{\numberline {2.4.2}Compared to other majors}{14}
\contentsline {subsubsection}{Similarities}{14}
\contentsline {subsubsection}{Differences}{14}
\contentsline {subsection}{\numberline {2.4.3}Roles}{14}
\contentsline {subsubsection}{Data Scientist}{14}
\contentsline {subsection}{\numberline {2.4.4}What will a Google Career in Computational Data Science look like?}{15}
\contentsline {subsubsection}{Salary and Payment}{15}
\contentsline {subsubsection}{Career Progression}{15}
\contentsline {subsubsection}{A look to the future}{15}
\contentsline {chapter}{\numberline {3}Work Environment}{16}
\contentsline {section}{\numberline {3.1}Google Perks}{16}
\contentsline {section}{\numberline {3.2}Google Culture}{16}
\contentsline {section}{\numberline {3.3}Being Part of Google}{17}
\contentsline {section}{\numberline {3.4}Streams}{18}
\contentsline {section}{\numberline {3.5}Interviews and Skills}{18}
